package com.dayamed.email.portmonitor.SendEmailPortMonitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SendEmailPortMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SendEmailPortMonitorApplication.class, args);
	}

}


